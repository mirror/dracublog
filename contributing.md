# Contributing

## Introduction

Thank you for your interest in contributing to this project!

## Bug Reports and Suggestions

Please open a new issue [here](https://git.earne.link/earnestma/dracublog/issues) or discuss on [earne.link Discussion](https://discuss.earne.link/t/earnest).

## Style Guide

See the [editor configuration file](.editorconfig) file for hints.

## Making Changes

- [Fork the repo](https://git.earne.link/earnestma/dracublog), clone and create a branch
- Add, commit your changes, push to branch
- [Create a PR](https://git.earne.link/earnestma/dracublog/pulls)

## Changelog

Loose [semantic versioning](https://semver.org/) is used. View the tags for a changelog or [changelog.md](changelog.md).
